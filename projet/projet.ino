//Libraries
#include <FastLED.h>//https://github.com/FastLED/FastLED
#include <Melody.h>//https://github.com/dualB/Melody
#include <Musician.h> //https://github.com/dualB/Musician
#include <MozziGuts.h>//https://github.com/sensorium/Mozzi
#include <Oscil.h> // oscillator template
#include <tables/sin2048_int8.h> // sine table for oscillator
//Constants
#define NUM_STRIPS 1
#define NUM_LEDS 60
#define BRIGHTNESS 10
#define LED_TYPE WS2811
#define COLOR_ORDER BRG//RGB
#define FASTLED_ALLOW_INTERRUPTS 0
//#define FASTLED_INTERRUPT_RETRY_COUNT 1
#define FRAMES_PER_SECOND 60
#define COOLING 55
#define SPARKING 120
#define CONTROL_RATE 128

//Parameters
//const int buzzerPin = 11; //via mozzi
const int STRIP_PIN  = 3;
const int PIN_TRIGGER = 6;
const int PIN_ECHO = 7;
const int ANALOG_PIN =  0;

//Variables
int musicStatus =0;
int val = 1;
int bright = 10;
int countHand = 0;
long durationCur;
float analogVal  = 0;
unsigned long previousMillis = 0;
unsigned long previousMillisLight = 0;
unsigned long previousMillisHand = 0;
unsigned long duration = 0;
unsigned long durationLight = 500;

bool gReverseDirection  = false;
bool isDown = true;
bool isMusic = false;

//Objects
CRGB leds[NUM_LEDS];
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin(SIN2048_DATA);
///music
//Melody melody("g<<r-d- | g<< r-d-(g<dg<b)-d<*r | c*<<r-a-c*<<r-a- |(c*<af#<a)-d<r", 140);

//Frere Jacques
String frereJaque = "((cdec)x2   (efgr)x2   ((gagf)-ec)x2     (c g_ c+)x2)*";
Melody melody(frereJaque, 120);

//Au Clair de la Lune
String clairLune =" ( (cccde+dr  ceddc+.r)x2  dddd (a+ar)_ dc(b a g+.r)_ cccde+dr ceddc+.r )*";



void setup() {
  //Init led strips
  Serial.begin(9600);
  FastLED.addLeds<LED_TYPE, STRIP_PIN, COLOR_ORDER>(leds, NUM_LEDS);
  FastLED.setBrightness(BRIGHTNESS);

  pinMode(PIN_TRIGGER, INPUT);
  pinMode(PIN_TRIGGER, OUTPUT);
  //pinMode(buzzerPin, OUTPUT);
  ledScenario();
  startMozzi(CONTROL_RATE);
}


void loop() {
  audioHook();
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillisLight >= durationLight) {
    previousMillisLight = currentMillis;
    uptLedStrip(detectlight());
  }
  detectHand();

}

void playMusic(void)
{
  if (isMusic)
  {
    if (melody.hasNext())
    {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis >= duration) {
        previousMillis = currentMillis;
        melody.next();
        int freq = melody.getFrequency();
        duration = melody.getDuration();

        aSin.setFreq(freq);
      }
    }
    else
    {
      melody.restart();
    }
    /*
        if (melody.hasNext()) {
          melody.next();
          int freq = melody.getFrequency();
          unsigned long duration = melody.getDuration();
          freq > 0 ? tone(buzzerPin, freq) : noTone(buzzerPin);
          delay(duration);
        }
        else
        {
          melody.restart();
        }
    */
  }
  else
  {
    aSin.setFreq(0);
  }
}

void detectHand(void)
{
  digitalWrite(PIN_TRIGGER, HIGH);
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillisHand >= 1000)
  {
    previousMillisHand = currentMillis;
    digitalWrite(PIN_TRIGGER, LOW);
    durationCur = pulseIn(PIN_ECHO, HIGH, 10000);
    if (durationCur < 400) {
      countHand++;
    }
    else
    {
      countHand = 0;
    }
    if (countHand >= 2) {//2sec
      musicStatus++;
      //Serial.println(musicStatus);
      if(musicStatus%3 == 0)
      {
        isMusic = false;
        musicStatus = 0;
      }
      else if(musicStatus%3 == 1)
      {
        melody.setScore(clairLune);
        isMusic = true; 
      }
      else if(musicStatus%3 == 2)
      {
        melody.setScore(frereJaque);
        melody.setTempo(120);
        isMusic = true;
      }
      //upt play sound here
      

    }
  }
}

bool detectlight(void)
{
  analogVal = analogRead(ANALOG_PIN);
  //Serial.println(analogVal);
  return (analogVal < 20);
}

void uptLedStrip(bool isLightUp)
{
  if (isLightUp)
  {
    if (!isDown)
    {
      ledScenario();
      isDown = true;
    }

    if (bright < 1)
    {
      val = -1;
    }
    if (bright > 9)
    {
      val = 1;
    }
    bright -= val;
    FastLED.setBrightness(bright);
    FastLED.show();
  }
  else
  {
    if (isDown)
    {
      for (int i = 0; i < NUM_LEDS; i++)
      {
        leds[i] = CRGB::Black;
        isDown = false;
      }
      FastLED.show();
    }
  }
}

void ledScenario(void )
{
  ////LEDS Strip scenario
  for (int i = 0; i < NUM_LEDS; i++)
  {
    leds[i].setRGB(255, 194, 0);
  }
}

//mozzi
void updateControl()
{
  playMusic();
}

int updateAudio() {
  return MonoOutput::from8Bit(aSin.next());
}
